var afficheNom = document.querySelector('#visiteurs');   
var buttonSendRapport = document.querySelector('#button')
var idSelected;
var visiteurs; 
var recupId;
var found;
const url = 'http://localhost:90/gsb/visiteur/';

// fetch de recuperation des visteurs 
function getVisiteurs() {
    fetch(url)                         // Le fetch va chercher l'instruction
    .then(response => response.json())
    .then((visiteurs) => {  
    this.visiteurs = visiteurs;                 // récupère le tableau d'objets
    visiteurs.forEach(visiteur => {             // Recuperation des visiteurs 
       console.log(visiteur);
       afficheNom.innerHTML += `<option value="${visiteur.id}">${visiteur.nom} </option>`;
    });       
    })  // parse la réponse en json
    .catch((error) => {                 // afficher une erreur
        console.log(`Voici mon erreur ${error}`);
    })
}   

getVisiteurs();










// fetch creation d'un rapport 


function creerRapport(modifRapport, bilanRapport, dateRapport) {
var urlCreationRapport           = `http://localhost:90/gsb/visiteur/${recupId}/rapport`;   

    fetch(urlCreationRapport, {
        method: 'POST',
        body: JSON.stringify({
            motif:  modifRapport,
            bilan:  bilanRapport, 
            date:   dateRapport 
            }), 
        headers: { 
                "Content-type": "application/json; charset=UTF-8"
            } 
    })                        
    .then(response => response.json())
    .then((data) => {        
    console.log(data); 
    })      
    .catch((error) => {                 // afficher une erreur
        console.log(`Voici mon erreur ${error}`);
    })
};       

    
// recuperation de L'id du visiteur selectionner 
afficheNom.addEventListener('change', (event) => {  
    recupId       = event.target.value;
 //   parseInt(this.recupId);
 console.log(recupId);
    //var found         = this.visiteurs.find(visiteur => visiteur.id == recupId) ;  
   // console.log(found);
});


//Recuperation du formulaire de la creation d'un rapport et creation d'un rapport
buttonSendRapport.addEventListener('click', (event) => {
    event.preventDefault(); 
var motifRapport     = document.querySelector('#motif').value;
var bilanRapport     = document.querySelector('#bilan').value; 
var dateRapport      = document.querySelector('#dateDuRapport').value; 

console.log(`voici le motif du rapport     : ${motifRapport}
            , voici le bilan du rapport    : ${bilanRapport} 
            , voici la date  du rapport    : ${dateRapport}`);

//appel de la methode fetch creation d'un rapport
creerRapport(motifRapport, bilanRapport, dateRapport);
});
